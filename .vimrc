" autocomplete like zsh/fish
set wildmenu
set wildmode=full

" command history
set history=500

" highlight all search terms
set hlsearch
